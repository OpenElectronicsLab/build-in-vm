#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2022 K. M. Shaw, E. K. Herman

import re
import urllib.request
from bs4 import BeautifulSoup


def get_debian_stable_version_and_codename():
    """Get the version and codename of the latest debian stable release.

    Loads the debian release webpage, finds a line that looks something like
    'Debian 11 ("bullseye") - current "stable" release' and extracts the
    codename and version number from this line.
    """
    debian_release_url = "https://www.debian.org/releases/"

    # load the release page
    with urllib.request.urlopen(debian_release_url) as response:
        html = response.read()
        status = response.status
    soup = BeautifulSoup(html, "html.parser")

    # find all rows, skipping the header row
    rows = soup.find_all("tr")
    for row in rows[1:]:
        cells = row.find_all("td")
        version_num = cells[0].get_text(strip=True)
        codename = cells[1].get_text(strip=True).lower()
        # status column is the last cell
        for q in cells[-1].find_all("q"):
            if q.string == "stable":
                return version_num, codename

    raise ValueError(
        f"Debian release not found - {debian_release_url} may "
        "have changed formatting"
    )


def get_latest_debian_cloud_datestamp_and_buildnum(codename):
    """Get the date and build number for the latest debian cloud image"""
    base_url = f"https://cloud.debian.org/images/cloud/{codename}/"
    with urllib.request.urlopen(base_url) as response:
        html = response.read()
        status = response.status
    soup = BeautifulSoup(html, "html.parser")

    versions = []
    for td in soup.find_all("td", **{"class": "indexcolname"}):
        href = td.find("a").get("href")
        matched = re.compile(r"([0-9]+)-([0-9]+)/").match(href)
        if matched:
            datestamp, buildnum = matched.groups()
            versions.append([datestamp, buildnum])
    versions.sort()
    return versions[-1]


def get_base_url_inner(codename, datestamp, buildnum):
    return f"cloud.debian.org/images/cloud/{codename}/{datestamp}-{buildnum}/"


def get_http_base_url(codename, datestamp, buildnum):
    return "http://" + get_base_url_inner(codename, datestamp, buildnum)


def get_https_base_url(codename, datestamp, buildnum):
    return "https://" + get_base_url_inner(codename, datestamp, buildnum)


def get_image_filename(debver, codename, datestamp, buildnum):
    return f"debian-{debver}-generic-amd64-{datestamp}-{buildnum}.qcow2"


def get_sha512_sum(base_url, image_filename):
    sha512sums_url = base_url + "SHA512SUMS"

    with urllib.request.urlopen(sha512sums_url) as response:
        for line in response.readlines():
            sha512sum, filename = (
                re.compile("([0-9a-f]*)\\s*([^\\s]*)")
                .match(line.decode("utf-8", "strict"))
                .groups()
            )
            if filename == image_filename:
                return sha512sum
    raise ValueError(f"File '{image_filename}' not found in {sha512sums_url}")


if __name__ == "__main__":
    debver, codename = get_debian_stable_version_and_codename()
    datestamp, buildnum = get_latest_debian_cloud_datestamp_and_buildnum(codename)
    http_base_url = get_http_base_url(codename, datestamp, buildnum)
    image_filename = get_image_filename(debver, codename, datestamp, buildnum)
    image_url = http_base_url + image_filename

    https_base_url = get_https_base_url(codename, datestamp, buildnum)
    sha512sum = get_sha512_sum(https_base_url, image_filename)

    with open("build-in-vm", "r", encoding="utf-8") as file:
        lines = file.readlines()
    for (i, line) in enumerate(lines):
        if line.startswith(f"\tDEBIAN_CLOUD_IMAGE_NAME="):
            lines[i] = f"\tDEBIAN_CLOUD_IMAGE_NAME={image_filename}\n"
        elif line.startswith(f"\tDEBIAN_CLOUD_IMAGE_URL="):
            lines[i] = f"\tDEBIAN_CLOUD_IMAGE_URL={image_url}\n"
        elif line.startswith(f"\tDEBIAN_CLOUD_IMAGE_SHA512SUM="):
            lines[i] = f"\tDEBIAN_CLOUD_IMAGE_SHA512SUM={sha512sum}\n"
    with open("build-in-vm", "w", encoding="utf-8") as file:
        file.writelines(lines)
