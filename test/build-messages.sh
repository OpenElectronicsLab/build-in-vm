#!/bin/bash
set -e
set -x
set -o pipefail

sudo apt-get update
sudo apt-get install -y expect

echo "Hello 1 from inside of ${HOSTNAME}!" > message1.txt
echo "" >> message1.txt
cat in1.txt >> message1.txt
expect -v | cut -f-2 -d ' ' >> message1.txt

echo "Hello 2 from inside of ${HOSTNAME}!" > message2.txt
cat in2.txt >> message2.txt
