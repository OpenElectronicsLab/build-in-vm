#!/bin/bash
if [ -d /sys/firmware/efi ]; then
    echo "Booted in UEFI mode" > bootmode.txt
else
    echo "Booted in BIOS mode" > bootmode.txt
fi
