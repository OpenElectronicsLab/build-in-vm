#!/bin/bash
# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2022-2024 Stichting Open Electronics Lab
# https://git.openelectronicslab.org/openelectronicslab/build-in-vm.git

VERSION=0.0.1

set -e
set -o pipefail

OPTIONS=$(getopt -o 'b:n:i:s:o:O:m:r:pd:UP:ujvVh' \
 --long 'base-qcow2:,vm-hostname:,file-in:,script:,file-out:,file-out-always:,mount-file:,resize-qcow2:,preserve-qcow2,vm-dir:,vm-ssh-user:,vm-ssh-user-hashed-passwd:,unrestricted,just-download-cloud-qcow2,uefi,uefi-code:,uefi-vars:,verbose,version,help' \
 -n "$0" -- "$@")

if [ $? -ne 0 ]; then
	echo 'Terminating...' >&2
	exit 1
fi

# Note the quotes around "$OPTIONS": they are essential!
eval set -- "$OPTIONS"
unset OPTIONS

while true; do
	case "$1" in
		'-b'|'--base-qcow2')
			BASE_QCOW2=$2
			shift 2
			continue
		;;
		'-n'|'--vm-hostname')
			VM_HOSTNAME=$2
			shift 2
			continue
		;;
		'-i'|'--file-in')
			FILES_IN="$FILES_IN $2"
			shift 2
			continue
		;;
		'-s'|'--script')
			SCRIPTS="$SCRIPTS $2"
			shift 2
			continue
		;;
		'-o'|'--file-out')
			FILES_OUT="$FILES_OUT $2"
			shift 2
			continue
		;;
		'-O'|'--file-out-always')
			FILES_OUT_ALWAYS="$FILES_OUT_ALWAYS $2"
			shift 2
			continue
		;;
		'-m'|'--mount-file')
			MOUNT_FILES="$MOUNT_FILES $2"
			shift 2
			continue
		;;
		'-p'|'--preserve-qcow2')
			PRESERVE_QCOW2=1
			shift 1
			continue
		;;
		'-r'|'--resize-qcow2')
			RESIZE_QCOW2=$2
			shift 2
			continue
		;;
		'-d'|'--vm-dir')
			VM_DIR=$2
			shift 2
			continue
		;;
		'-U'|'--vm-ssh-user')
			VM_SSH_USER=$2
			shift 2
			continue
		;;
		'-P'|'--vm-ssh-user-hashed-passwd')
			VM_SSH_USER_HASHED_PASSWD=$2
			shift 2
			continue
		;;
		'-u'|'--unrestricted')
			UNRESTRICTED=1
			shift 1
			continue
		;;
		'-j'|'--just-download-cloud-qcow2')
			JUST_DOWNLOAD_CLOUD_QCOW2=1
			shift 1
			continue
		;;
		'--uefi')
			UEFI=1
			shift 1
			continue
		;;
		'--uefi-code')
			UEFI_CODE=$2
			shift 2
			continue
		;;
		'--uefi-vars')
			UEFI_VARS=$2
			shift 2
			continue
		;;
		'-v'|'--verbose')
			VERBOSE=1
			shift 1
			continue
		;;
		'-V'|'--version')
			VERSION_ASKED=1
			shift 1
			continue
		;;
		'-h'|'--help')
			HELP_ASKED=1
			shift 1
			continue
		;;
		'--')
			shift
			break
		;;
		*)
			echo 'Internal error!' >&2
			exit 1
		;;
	esac
done

# $ man test | grep -A1 '\-n \|\-z '
#       -n STRING
#              the length of STRING is nonzero
# --
#       -z STRING
#              the length of STRING is zero

if [ -n "$VERBOSE" ] && [ "$VERBOSE" -gt 0 ]; then
	set -x
	RM_VERBOSE=-v
fi

OUR_NAME=$(basename $0)

if [ -n "$VERSION_ASKED" ]; then
	echo "$OUR_NAME $VERSION"
	exit 0
fi

if [ -z "$DEBIAN_CLOUD_IMAGE_NAME" ]; then
	# update with build-in-vm/update-cloud-info.py
	DEBIAN_CLOUD_IMAGE_NAME=debian-12-generic-amd64-20241201-1948.qcow2
fi
if [ -z "$DEBIAN_CLOUD_IMAGE_SHA512SUM" ]; then
	# update with build-in-vm/update-cloud-info.py
	DEBIAN_CLOUD_IMAGE_SHA512SUM=94db5b062486d2070c712bd96a7a85e5873ec17fc17bad3262639f9974a3491e13695fac1e72b006c20014b12b7bf211b017059f5b3e41d5da24990a7c1cc332
fi
if [ -z "$DEBIAN_CLOUD_IMAGE_URL" ]; then
	# update with build-in-vm/update-cloud-info.py
	DEBIAN_CLOUD_IMAGE_URL=http://cloud.debian.org/images/cloud/bookworm/20241201-1948/debian-12-generic-amd64-20241201-1948.qcow2
fi

UEFI_CODE_FILE=/usr/share/OVMF/OVMF_CODE.fd
UEFI_VARS_FILE=/usr/share/OVMF/OVMF_VARS.fd
function usage() {
	TEST_FILE=$(find . -name 'in1.txt' 2>/dev/null || true)
	TEST_DIR=$(dirname $TEST_FILE 2>/dev/null || true)
	if [ -z "$TEST_DIR" ]; then
		TEST_DIR="\$HOME"
	fi
	cat << EOF
$OUR_NAME version $VERSION
	$NAME is a tool for running a build in a VM.
	once the vm is initialized, via cloud-init
	first, the --file-in files, if any, are copied to the VM
	next, the script files are copied to the remote machine and executed
		if multiple --script are specified they will be executed
		in the order given
	finally, the --file-out files, if any, are copied from the VM
OPTIONS:
	--base-qcow2 is the backing .qcow2 image, cloud-init support expected
		defaults to: $DEBIAN_CLOUD_IMAGE_NAME
		see also:
		https://cloud.debian.org/images/cloud/
	--vm-hostname is optional, defaults to "vm"
	--unrestricted is optional, allows network connections to the world
		required for "apt get"
	--file-in, supports a from:to colon-separated syntax
		if a colon is not specified, then the working directory
		and basename is used for the target
	--script executable, to be run in the VM
	--file-out, files to be copied out after --script files are run
		supports a from:to colon-separated syntax
	--file-out-always, like --file-out, except will be copied out even
		if the specified --script exits with an error
	--mount-file, files to be mounted by qemu, via:
		-drive if=virtio,format=raw,file=\$MOUNT_FILE
	--vm-dir is optional, to specify the location of the new .qcow2 file
		and supporting files
        --vm-ssh-user to specify the user name expected by the qcow2 image
        --vm-ssh-user-hashed-passwd to specify the password hash as generated
		by mkpasswd(1); --vm-ssh-user=root is a requirement for this
	--preserve-qcow2 is optional, to retain the .qcow2 after success
	--resize-qcow2 is optional, e.g.: +2G
	--uefi is optional to boot the VM in UEFI mode (default: BIOS mode)
	--uefi-code (default: $UEFI_CODE_FILE)
	--uefi-vars (default: $UEFI_VARS_FILE)
	--help prints this message
	--version  ( $VERSION )
	--verbose

EXAMPLE USAGE:
$0 \\
	--base-qcow2=./debian-cloud.qcow2 \\
	--vm-hostname=foo \\
	--unrestricted \\
	--file-in=$TEST_DIR/in1.txt:/home/debian/in1.txt \\
	--file-in=$TEST_DIR/in2.txt \\
	--script=$TEST_DIR/build-messages.sh \\
	--file-out=/home/debian/message1.txt \\
	--file-out=message2.txt:$PWD/message2.txt \\
	--vm-dir=vm-build \\
	--preserve-qcow2
EOF
}

if [ -n "$HELP_ASKED" ]; then
	usage
	exit 0
fi

if [ -z "$PRESERVE_QCOW2" ]; then
	PRESERVE_QCOW2=0
fi

if [ -z "$CLOUD_IMAGES_DIR" ]; then
	CLOUD_IMAGES_DIR=cloud-images
fi

function download-debian-cloud-image()
{
	mkdir -pv "$CLOUD_IMAGES_DIR"
	pushd "$CLOUD_IMAGES_DIR"

	# check that we have the correct .sha512sums file
	if grep "$DEBIAN_CLOUD_IMAGE_SHA512SUM[ ]\+$DEBIAN_CLOUD_IMAGE_NAME" \
		$DEBIAN_CLOUD_IMAGE_NAME.sha512sums 2>/dev/null ; then

		# check if the qcow2 sha512sum matches
		if sha512sum --check $DEBIAN_CLOUD_IMAGE_NAME.sha512sums; then
			popd
			ls -l "$CLOUD_IMAGES_DIR/$DEBIAN_CLOUD_IMAGE_NAME"
			return 0
		fi
	fi

	echo $DEBIAN_CLOUD_IMAGE_URL
	rm -fv $DEBIAN_CLOUD_IMAGE_NAME debian-cloud.qcow2
	echo "environment proxy variables:"
	env | grep -i proxy || true
	# download to temp name
	curl --output $DEBIAN_CLOUD_IMAGE_NAME.tmp \
		-L $DEBIAN_CLOUD_IMAGE_URL
	# check against temp name
	echo "$DEBIAN_CLOUD_IMAGE_SHA512SUM  $DEBIAN_CLOUD_IMAGE_NAME.tmp" \
		> $DEBIAN_CLOUD_IMAGE_NAME.tmp.sha512sums
	sha512sum --check $DEBIAN_CLOUD_IMAGE_NAME.tmp.sha512sums
	# passed, so remove old sha512sums
	rm $DEBIAN_CLOUD_IMAGE_NAME.tmp.sha512sums
	# and move to real name
	mv -v $DEBIAN_CLOUD_IMAGE_NAME.tmp $DEBIAN_CLOUD_IMAGE_NAME
	# re-create .sha512sums with current name
	echo "$DEBIAN_CLOUD_IMAGE_SHA512SUM  $DEBIAN_CLOUD_IMAGE_NAME" \
		> $DEBIAN_CLOUD_IMAGE_NAME.sha512sums
	# make read-only
	chmod -v a-w $DEBIAN_CLOUD_IMAGE_NAME
	ln -svf $DEBIAN_CLOUD_IMAGE_NAME debian-cloud.qcow2
	popd
}

if [ -z "$JUST_DOWNLOAD_CLOUD_QCOW2" ]; then
	JUST_DOWNLOAD_CLOUD_QCOW2=0
fi

if [ $JUST_DOWNLOAD_CLOUD_QCOW2 -gt 0 ]; then
	download-debian-cloud-image
	exit 0
fi

# if nothing is specified, just print the usage and exit
if [ -z "$SCRIPTS" ] && \
   [ -z "$FILES_IN" ] && \
   [ -z "$FILES_OUT" ] && \
   [ -z "$FILES_OUT_ALWAYS" ] && \
   [ $PRESERVE_QCOW2 -eq 0 ] ; then
	usage
	exit 1
fi

if [ -z "$VM_SSH_USER" ]; then
	VM_SSH_USER=debian
fi

if [ -n "$VM_SSH_USER_HASHED_PASSWD" ] &&
   [ "$VM_SSH_USER" != "root" ]; then
	usage
	echo
	echo "--vm-ssh-user-hashed-passwd requires --vm-ssh-user=root"
	echo
	exit 1
fi

DEFAULT_BASE_QCOW2=$CLOUD_IMAGES_DIR/$DEBIAN_CLOUD_IMAGE_NAME
if [ -z "$BASE_QCOW2" ]; then
	BASE_QCOW2=$DEFAULT_BASE_QCOW2
fi

if [ ! -f $BASE_QCOW2 ] ; then
	BASENAME_BASE_QCOW2="$(basename $BASE_QCOW2)"
	if [ "$BASENAME_BASE_QCOW2" == "$DEBIAN_CLOUD_IMAGE_NAME" ] || \
	   [ "$BASENAME_BASE_QCOW2" == "debian-cloud.qcow2" ]; then
		download-debian-cloud-image
		if [ "$BASE_QCOW2" != "$DEFAULT_BASE_QCOW2" ]; then
			mkdir -pv $(dirname $BASE_QCOW2)
			ln -svf $(readlink -f "$DEFAULT_BASE_QCOW2") \
				"$BASE_QCOW2"
		fi
	fi
fi
if [ ! -f $BASE_QCOW2 ]; then
	LATEST_URL=$(echo "$DEBIAN_CLOUD_IMAGE_URL" | sed -e 's@20[0-9]*.*@latest/@g')
	LATEST_QCOW2=$(echo "$DEBIAN_CLOUD_IMAGE_URL" | sed -e 's@.*\(debian-[0-9]*-genericcloud-amd64\)-.*@\1.qcow2@g')
	usage
	echo
	echo "--base-qcow2 $BASE_QCOW2 not found."
	echo
	echo "    see: $LATEST_URL"
	echo "  qcow2: $LATEST_QCOW2"
	echo "    and: SHA512SUMS"
	echo
	exit 1
fi

if [ -z "$VM_HOSTNAME" ]; then
	VM_HOSTNAME=vm
fi

if [ -z "$VM_BASE_DIR" ]; then
	VM_BASE_DIR=vm
fi

VM_DIR_CREATED_BY_SCRIPT=0
# VM_DIR is where files which may be preserved will go
# we will preserve files if the build fail
if [ -z "$VM_DIR" ]; then
	VM_DIR="$VM_BASE_DIR/$VM_HOSTNAME"
fi
if [ ! -d $VM_DIR ]; then
	mkdir -pv $VM_DIR
	VM_DIR_CREATED_BY_SCRIPT=1
fi
if [ $VM_DIR_CREATED_BY_SCRIPT -ne 0 ]; then
	echo "$VM_DIR created by $OUR_NAME"
fi
ls -ld $VM_DIR

# VM_TMP_DIR is where any sort of intermediate file may reside
if [ -z "$VM_TMP_DIR" ]; then
	VM_TMP_DIR="$VM_DIR/tmp"
fi
mkdir -pv $VM_TMP_DIR

if [ -n "$UNRESTRICTED" ] && [ "$UNRESTRICTED" -gt 0 ]; then
	RESTRICT=",restrict=off"
else
	RESTRICT=",restrict=on"
fi

VM_BACKING_QCOW2=$VM_DIR/$VM_HOSTNAME.backing.qcow2
VM_QCOW2=$VM_DIR/$VM_HOSTNAME.qcow2
VM_SSH_CFG=$VM_QCOW2.ssh_config
VM_SSH_KEY=$VM_QCOW2.ssh_id
VM_PID_FILE=$VM_QCOW2.pid

if [ -z "$VM_SSH_HOST_KEY" ]; then
	VM_SSH_HOST_KEY=$VM_TMP_DIR/$(basename $VM_QCOW2).ssh_host_key
fi
VM_LOG=$VM_TMP_DIR/$(basename $VM_QCOW2).0.log

function free-port() {
	perl -E		'use IO::Socket::INET;
			 my $s = IO::Socket::INET->new(Listen => 1);
			 say $s->sockport;
			 $s->close()' \
		2>/dev/null \
	|| ruby -e	'require "socket";
			 s = TCPServer.new("", 0);
			 print s.addr[1], "\n";
			 s.close()' \
		2>/dev/null \
	|| node -e	'"use strict";
			 const http = require("http");
			 let server = http.createServer();
			 var listener = server.listen(0, function() {
				console.log(listener.address().port);
				listener.close();
			 });' \
		2>/dev/null \
	|| python -c	'import socket; \
			 s = socket.socket(); \
			 s.bind(("", 0)); \
			 print(s.getsockname()[1]); \
			 s.close()'
}
VM_SSH_PORT=$( free-port )

function pid-running() {
	kill -0 $1 2>/dev/null && echo 1 || echo 0
}

BUILD_SUCCESS=0
function cleanup() {
	WAIT_LOOPS=0;
	MAX_WAIT_LOOPS=10
	echo "VM_PID: $VM_PID"
	while [ "$(pid-running $VM_PID)" -ne 0 ] \
		&& [ $WAIT_LOOPS -lt $MAX_WAIT_LOOPS ]; do
		WAIT_LOOPS=$(( $WAIT_LOOPS + 1 ))
		echo "trying ssh shutdown ($WAIT_LOOPS of $MAX_WAIT_LOOPS)"
		ssh -F $VM_SSH_CFG vm $VM_SUDO shutdown -h now || true
		echo "waiting for vm to shutdown..."
		sleep 2
	done
	if [ "$(pid-running $VM_PID)" -ne 0 ]; then
		echo "VM_PID $VM_PID still running, issuing kill"
		kill $VM_PID || true
	else
		rm $RM_VERBOSE -f $VM_PID_FILE
	fi

	if [ $BUILD_SUCCESS -eq 0 ]; then
		echo -e "\n\nBUILD FAILED! see $VM_LOG\n"
	fi

	if [ $BUILD_SUCCESS -eq 0 ] || [ -n "$PRESERVE_VM_LOG" ]; then
		echo "     VM_LOG: $VM_LOG"
		echo "     VM_DIR: $VM_DIR"
		echo " VM_TMP_DIR: $VM_TMP_DIR"
		echo "launch with: $VM_QCOW2.sh"
	else
		rm $RM_VERBOSE -r -f $VM_TMP_DIR
		if [ $PRESERVE_QCOW2 -eq 0 ]; then
			if [ $VM_DIR_CREATED_BY_SCRIPT -gt 0 ]; then
				rm $RM_VERBOSE -rf $VM_DIR
			else
				rm $RM_VERBOSE $VM_QCOW2 $VM_QCOW2.*
			fi
			echo "cleanup complete"
		else
			echo "launch with: $VM_QCOW2.sh"
		fi
	fi
}
trap cleanup EXIT # kill the vm and eliminate temp-dir on script exit

if ! command -v qemu-system-x86_64 >/dev/null 2>&1; then
	cat >&2 << 'EOF'

	====================================================
	qemu-system-x86_64(1) not in PATH
		Is the "qemu-system-x86" package installed?
	https://packages.debian.org/stable/qemu-system-x86
	====================================================

EOF
	exit 1
fi

if ! command -v qemu-img >/dev/null 2>&1; then
	cat >&2 << 'EOF'

	====================================================
	qemu-img(1) not in PATH
		Is the "qemu-utils" package installed?
	https://packages.debian.org/stable/qemu-utils
	====================================================

EOF
	exit 1
fi

if [ -z "$UEFI_CODE" ]; then
	UEFI_CODE=$UEFI_CODE_FILE
fi
if [ -z "$UEFI_VARS" ]; then
	UEFI_VARS=$UEFI_VARS_FILE
fi

if [ -n "$UEFI" ] && [ "$UEFI" -gt 0 ] && ![ -f $UEFI_CODE ]; then
	cat >&2 << 'EOF'

	====================================================
	UEFI bios file not found at $UEFI_CODE
		Is the "ovmf" package installed?
	https://packages.debian.org/stable/ovmf
	====================================================

EOF
	exit 1
fi

ln -sfv $(readlink -f $BASE_QCOW2) $VM_BACKING_QCOW2
qemu-img create -f qcow2 -F qcow2 -b $(basename $VM_BACKING_QCOW2) $VM_QCOW2
if [ -n "$RESIZE_QCOW2" ]; then
	qemu-img resize $VM_QCOW2 "$RESIZE_QCOW2"
fi

mkdir -pv $(dirname $VM_QCOW2)

FULL_PATH_QCOW2=$(readlink -f $VM_QCOW2)
SSH_KNOWN_HOSTS_FILE=$FULL_PATH_QCOW2.ssh_known_hosts

# We don't need to use sudo if we're logging in as root
if [ "$VM_SSH_USER" == "root" ]; then
	VM_SUDO=
else
	VM_SUDO=sudo
fi

SSH_IDENTITY_FILE=$(readlink -f $VM_SSH_KEY)
cat << EOF > $VM_SSH_CFG
Host vm
    Hostname 127.0.0.1
    Port $VM_SSH_PORT
    HostKeyAlias $VM_HOSTNAME
    User $VM_SSH_USER
    IdentityFile $SSH_IDENTITY_FILE
    IdentitiesOnly yes
    UserKnownHostsFile $SSH_KNOWN_HOSTS_FILE
    StrictHostKeyChecking yes
EOF

if [ -n "$http_proxy" ]; then
    cat << EOF >> $VM_SSH_CFG
    SendEnv http_proxy
EOF

    if ( echo "$http_proxy" | grep 'localhost\|127\.0\.0\.' ); then
        # http://user:proxy@localhost:3128/
	PROXY_HOST_PORT=$(echo "$http_proxy" \
		| cut -f3 -d'/' \
		| sed -e 's/.*@//')
        cat << EOF >> $VM_SSH_CFG
    RemoteForward $PROXY_HOST_PORT $PROXY_HOST_PORT
EOF
    fi
fi

if [ ! -f $VM_SSH_KEY ]; then
	echo "ssh-keygen for $VM_SSH_KEY"
	ssh-keygen -q \
		-f $VM_SSH_KEY \
		-t ed25519 \
		-N "" \
		-C "$(basename $VM_SSH_KEY)"
fi
echo "VM_SSH_KEY: $VM_SSH_KEY"

if [ ! -f $VM_SSH_HOST_KEY ]; then
	echo "ssh-keygen for $VM_SSH_HOST_KEY"
	ssh-keygen -q \
		-f $VM_SSH_HOST_KEY \
		-t ed25519 \
		-N "" \
		-C "$(basename $VM_SSH_HOST_KEY)"
fi
echo "$VM_HOSTNAME $(cat $VM_SSH_HOST_KEY.pub)" \
	> $SSH_KNOWN_HOSTS_FILE
echo "VM_SSH_HOST_KEY: $VM_SSH_HOST_KEY"

cat <<EOF > $VM_TMP_DIR/user-data
#cloud-config

ssh_authorized_keys:
  - $(cat $VM_SSH_KEY.pub)

ssh_keys:
  ed25519_private: |
$(cat $VM_SSH_HOST_KEY | sed -e 's/^/    /g')

  ed25519_public: |
    $(cat $VM_SSH_HOST_KEY.pub)

ssh:
  emit_keys_to_console: true

# ssh_genkeytypes: []
# Cloud config schema errors: ssh_genkeytypes: [] is too short
ssh_genkeytypes: ['ecdsa']

$( [[ $VM_SSH_USER == "root" ]] && echo "disable_root: false" )

# bootcmd on debian runs in "dash", thus "echo" is like bash "echo -e"
bootcmd:
  - echo "\n---------------------\n ( start bootcmd )\n---------------------\n"
  - echo "Do not be alarmed by earlier GROWROOT errors, like:"
  - echo "        growpart, grep not found"
  - echo "        instead, see debian Bug#1037914"
  - echo "\nhttps://www.mail-archive.com/debian-bugs-dist@lists.debian.org/msg1909539.html\n"
  - ls -l /bin/grep /bin/sed /bin/rm /bin/awk
  - echo "\n--------\n cloud-init schema --system\n--------\n"
  - cloud-init schema --system
  - echo "--------"
  - echo "\n---------------------\n (  end  bootcmd )\n---------------------\n"

runcmd:
  - echo "\n---------------------\n ( start runcmd  )\n---------------------\n"
  - cp -v /etc/ssh/sshd_config /etc/ssh/sshd_config.orig
  - echo "enabling http_proxy for ssh connections"
  - sed -i -e's/^AcceptEnv /AcceptEnv http_proxy /' /etc/ssh/sshd_config
  - diff -u /etc/ssh/sshd_config.orig /etc/ssh/sshd_config
  - echo "systemctl restart sshd.service"
  - systemctl restart sshd.service
  - echo "\n---------------------\n (  end  runcmd  )\n---------------------\n"
EOF

if [[ -n "$VM_SSH_USER_HASHED_PASSWD" ]]; then
cat <<EOF >> $VM_TMP_DIR/user-data

users:
  - name: $VM_SSH_USER
    lock_passwd: false
    hashed_passwd: '$VM_SSH_USER_HASHED_PASSWD'
EOF
fi

cat <<EOF > $VM_TMP_DIR/meta-data
instance-id: iid-$VM_HOSTNAME
local-hostname: $VM_HOSTNAME
EOF

if command -v genisoimage >/dev/null 2>&1; then
	GENISO_CLOUD_INIT=$VM_TMP_DIR/cloud-init-seed.genisoimage.iso
	genisoimage -output $GENISO_CLOUD_INIT -volid cidata -joliet -rock \
		$VM_TMP_DIR/user-data \
		$VM_TMP_DIR/meta-data \
		|| true
	if [ -f "$GENISO_CLOUD_INIT" ]; then
		CLOUD_INIT=$GENISO_CLOUD_INIT
	fi
fi

if [ -z "$CLOUD_INIT" ] && command -v cloud-localds >/dev/null 2>&1; then
	echo "trying cloud-localds"
	LOCALDS_CLOUD_INIT=$VM_TMP_DIR/cloud-init-seed.cloud-localds.iso
	cloud-localds $LOCALDS_CLOUD_INIT \
		$VM_TMP_DIR/user-data \
		$VM_TMP_DIR/meta-data \
		|| true
	if [ -f "$LOCALDS_CLOUD_INIT" ]; then
		CLOUD_INIT=$LOCALDS_CLOUD_INIT
	fi
fi
if [ -z "$CLOUD_INIT" ]; then
	echo "no cloud init seed file"

	if ! command -v genisoimage >/dev/null 2>&1; then
		cat >&2 << 'EOF'

	====================================================
	genisoimage(1) not in PATH
		Is the "genisoimage" package installed?
	https://packages.debian.org/stable/genisoimage
	====================================================

EOF
	fi
	exit 1;
fi

rm $RM_VERBOSE -f $VM_QCOW2.known_hosts.check

if [ -n "$UEFI" ] && [ "$UEFI" -gt 0 ]; then
	UEFI_ARGS="-drive if=pflash,format=raw,readonly=on,file=$UEFI_CODE"
	UEFI_ARGS+=" -drive if=pflash,format=raw,readonly=on,file=$UEFI_VARS"
else
	UEFI_ARGS=""
fi

HOST_FWD="hostfwd=tcp:127.0.0.1:$VM_SSH_PORT-:22$RESTRICT"
EXTRA_QEMU_ARGS="-drive if=virtio,format=raw,file=$CLOUD_INIT"

USER_QEMU_MOUNTS=""
for file in $MOUNT_FILES; do
	if [ -z "$USER_QEMU_MOUNTS" ]; then
		USER_QEMU_MOUNTS="-drive if=virtio,format=raw,file=$file"
	else
		USER_QEMU_MOUNTS="$USER_QEMU_MOUNTS \\
 -drive if=virtio,format=raw,file=$file"
	fi
done

cat << EOF > $VM_QCOW2.sh
#!/bin/bash
# SPDX-License-Identifier: GPL-3.0-or-later
# $VM_QCOW2.sh
# generated by $OUR_NAME
# https://git.openelectronicslab.org/openelectronicslab/build-in-vm.git
# Copyright (C) 2022-2024 Stichting Open Electronics Lab

# $ man test
#       -n STRING
#              the length of STRING is nonzero
#       -z STRING
#              the length of STRING is zero
#       INTEGER1 -gt INTEGER2
#              INTEGER1 is greater than INTEGER2
# $ help set
#      -x  Print commands and their arguments as they are executed.

if [ -n "\$VERBOSE" ] && [ "\$VERBOSE" -gt 0 ]; then
	set -x
fi

VM_DIR=\$(dirname \$0)
QCOW2=\$VM_DIR/$(basename $VM_QCOW2)
SSH_CFG=\$VM_DIR/$(basename $VM_SSH_CFG)
PID_FILE=\$VM_DIR/$(basename $VM_PID_FILE)

# echo VM_DIR=\$VM_DIR
# echo QCOW2=\$QCOW2
# echo SSH_CFG=\$SSH_CFG
# echo PID_FILE=\$PID_FILE

if [ -z "\$VM_LOG" ]; then
	LOG_SPECIFIED=0
	# https://www.rfc-editor.org/rfc/rfc3339#section-5.6
	LOG_TIME=\$(date --utc +%Y-%m-%d_%H:%M:%SZ)
	if [ -n "\$VM_TMP_DIR" ]; then
		LOG_DIR=\$VM_TMP_DIR
	else
		LOG_DIR=\$VM_DIR/tmp
	fi
	VM_LOG=\$LOG_DIR/\$(basename \$QCOW2).\$LOG_TIME.log
	# echo LOG_DIR=\$LOG_DIR
	# echo VM_LOG=\$VM_LOG
else
	LOG_SPECIFIED=1
fi

mkdir -pv \$(dirname "\$VM_LOG")

if [ -z "\$QEMU_INTERACTIVE" ]; then
# QEMU_DISPLAY="-display none -serial file:\$VM_LOG"
QEMU_MONITOR_SOCK="\$QCOW2.monitor.sock"
QEMU_CONSOLE_SOCK="\$QCOW2.console.sock"
QEMU_DISPLAY="-display none"
QEMU_DISPLAY+=" -chardev socket,path=\$QEMU_CONSOLE_SOCK,server=on,wait=off"
QEMU_DISPLAY+=",id=char0,mux=on,logfile=\$VM_LOG"
QEMU_DISPLAY+=" -serial chardev:char0"
QEMU_DISPLAY+=" -monitor unix:\$QEMU_MONITOR_SOCK,server,nowait"
else
QEMU_DISPLAY="-nographic"
QEMU_DISPLAY+=" -chardev stdio,id=char0,mux=on,logfile=\$VM_LOG,signal=off"
QEMU_DISPLAY+=" -serial chardev:char0 -mon chardev=char0"
fi

QEMU_CMD="qemu-system-x86_64 \\
 -machine accel=kvm:tcg,type=q35 \\
 -smp 1 \\
 -m 2G \\
 \$QEMU_DISPLAY \\
 -device virtio-net-pci,netdev=net0 \\
 -netdev user,id=net0,$HOST_FWD \\
 $UEFI_ARGS \\
 -drive if=virtio,format=qcow2,file=\$QCOW2 \\
 $USER_QEMU_MOUNTS \\
 \$EXTRA_QEMU_ARGS \\
"

echo
echo \$QEMU_CMD
echo

if [ -n "\$QEMU_INTERACTIVE" ]; then
        \$QEMU_CMD
        exit \$?
fi

\$QEMU_CMD &
echo \$! > \$PID_FILE
VM_PID=\$(cat \$PID_FILE)
echo
echo "vm: \$QCOW2"
echo "pid: \$VM_PID"
echo "log: \$VM_LOG"
if [ -n "\$QEMU_CONSOLE_SOCK" ]; then
	echo
	echo "console socket: \$QEMU_CONSOLE_SOCK"
	echo "          e.g.: socat STDIO,cfmakeraw UNIX:\$QEMU_CONSOLE_SOCK"
fi
if [ -n "\$QEMU_MONITOR_SOCK" ]; then
	echo
	echo " qemu mon sock: \$QEMU_MONITOR_SOCK"
	echo "          e.g.: socat STDIO,cfmakeraw UNIX:\$QEMU_MONITOR_SOCK"
	echo "  see 'savevm' at"
        echo "    https://qemu-project.gitlab.io/qemu/system/monitor.html"
fi
echo

if [ -n "\$SKIP_SSH" ] && [ "\$SKIP_SSH" -gt 0 ]; then
	echo "connect with 'ssh -F \$SSH_CFG vm'"
	exit 0
fi

WAIT_LOOPS=0
MAX_WAIT_LOOPS=10
while [ \$WAIT_LOOPS -lt \$MAX_WAIT_LOOPS ] && \\
		! ssh -F \$SSH_CFG vm true 2>/dev/null; do
	WAIT_LOOPS=\$(( \$WAIT_LOOPS + 1 ))
	echo "waiting for vm (\$WAIT_LOOPS of \$MAX_WAIT_LOOPS)"
	sleep 1
done
if [ \$WAIT_LOOPS -lt \$MAX_WAIT_LOOPS ]; then
	echo "vm up"
fi

echo
echo "attempting to connect..."
echo
echo "ssh -F \$SSH_CFG vm \$@"
echo
ssh -F \$SSH_CFG vm "\$@"
echo
if [ -z "\$SKIP_WAIT_FOR_SHUTDOWN" ]; then
	echo "waiting for vm to shutdown..."
	ssh -F \$SSH_CFG vm $VM_SUDO shutdown -h now
	wait
fi
echo "VM_PID: \$VM_PID"
if kill -0 \$VM_PID 2>/dev/null; then
	echo "vm \$QCOW2 still running."
	echo "        pid: \$VM_PID"
	echo "        log: \$VM_LOG"
	echo "connect with:"
	echo "        ssh -F \$SSH_CFG vm"
	echo "disconnect with:"
	echo "        $VM_SUDO shutdown -h && exit"
	exit 1
fi
rm -fv \$PID_FILE

# If VM_LOG was specified, or if PRESERVE_VM_LOG is set, then
# the caller is responsible for cleaning it up
if [ \$LOG_SPECIFIED -eq 0 ] && [ -z "\$PRESERVE_VM_LOG" ]; then
	rm -fv "\$VM_LOG"
fi
EOF
chmod +x $VM_QCOW2.sh
SKIP_SSH=1 \
 VM_LOG="$VM_LOG" \
 EXTRA_QEMU_ARGS="$EXTRA_QEMU_ARGS" \
 $VM_QCOW2.sh
VM_PID=$(cat $VM_PID_FILE)
echo "VM_PID: $VM_PID"
printf "Waiting for vm to boot"
sleep 1

HOST_KEY_BLOCK_START="-----BEGIN SSH HOST KEY KEYS-----"
HOST_KEY_BLOCK_END="-----END SSH HOST KEY KEYS-----"
while ! grep -q "^$HOST_KEY_BLOCK_END" $VM_LOG; do
	printf "."
	sleep 1
done
echo ""

KNOWN_HOSTS_CHECK=$VM_TMP_DIR/$(basename $VM_QCOW2).known_hosts.check
sed -n "/$HOST_KEY_BLOCK_START/,/$HOST_KEY_BLOCK_END/p" \
		$VM_LOG \
	| head -n-1 \
	| tail -n+2 \
	| sed "s/^/$VM_HOSTNAME /g" \
	> $KNOWN_HOSTS_CHECK
diff -uw $SSH_KNOWN_HOSTS_FILE $KNOWN_HOSTS_CHECK

echo ""
echo "remote working directory:"
ssh -F $VM_SSH_CFG vm pwd

echo "copying files in"
for F in $FILES_IN; do
	F_local=`echo "$F" | cut -d':' -f1`
	F_remote=`echo "$F" | cut --only-delimited -d':' -f2`
	if [ -z "$F_remote" ]; then
		F_remote=$(basename $F_local)
	fi
	scp -F $VM_SSH_CFG $F_local vm:$F_remote
done

SCRIPT_ERROR=0
echo "running scripts"
for S in $SCRIPTS; do
	SCRIPT_NAME=$(basename $S)
	scp -F $VM_SSH_CFG $S vm:$SCRIPT_NAME
	ssh -F $VM_SSH_CFG vm ./$SCRIPT_NAME || SCRIPT_ERROR=$?
	if [ $SCRIPT_ERROR != 0 ]; then
		echo "$SCRIPT_NAME exited with error $SCRIPT_ERROR"
		break
	fi
done

if [ -n "$FILES_OUT_ALWAYS" ]; then
	echo "retrieving --file-out-always files"
fi
for F in $FILES_OUT_ALWAYS; do
	F_remote=`echo "$F" | cut -d':' -f1`
	F_local=`echo "$F" | cut --only-delimited -d':' -f2`
	if [ -z "$F_local" ]; then
		F_local=$(basename $F_remote)
	fi
	mkdir -pv $(dirname $F_local)
	scp -F $VM_SSH_CFG vm:$F_remote $F_local || SCRIPT_ERROR=$?
done
if [ $SCRIPT_ERROR != 0 ]; then
	exit $SCRIPT_ERROR
fi

echo "retrieving files out"
for F in $FILES_OUT; do
	F_remote=`echo "$F" | cut -d':' -f1`
	F_local=`echo "$F" | cut --only-delimited -d':' -f2`
	if [ -z "$F_local" ]; then
		F_local=$(basename $F_remote)
	fi
	mkdir -pv $(dirname $F_local)
	scp -F $VM_SSH_CFG vm:$F_remote $F_local
done

for S in $SCRIPTS; do
	SCRIPT_NAME=$(basename $S)
	ssh -F $VM_SSH_CFG vm rm -vf ./$SCRIPT_NAME
done;
ssh -F $VM_SSH_CFG vm $VM_SUDO cloud-init clean --logs

echo "clearing /etc/machine-id"
ssh -F $VM_SSH_CFG vm $VM_SUDO \
	rm -rvf /etc/machine-id
ssh -F $VM_SSH_CFG vm $VM_SUDO \
	touch --date=1970-01-01T00:00:00Z /etc/machine-id
ssh -F $VM_SSH_CFG vm $VM_SUDO \
	ls -l /etc/machine-id

echo "waiting for vm to shutdown..."
ssh -F $VM_SSH_CFG vm $VM_SUDO shutdown -h now
wait
BUILD_SUCCESS=1
