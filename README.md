<!-- SPDX-License-Identifier: GPL-3.0-or-later -->
<!--Copyright (C) 2022 S. K. Medlock, E. K. Herman, K. M. Shaw -->

# build-in-vm

This is a command-line utility for building software in vm-isolation.

## Example usage

```
./build-in-vm \
	--file-in=test/in1.txt \
	--file-in=test/in2.txt \
	--script=test/build-messages.sh \
	--file-out=message1.txt \
	--file-out=message2.txt

ls -l message1.txt message2.txt
```

First, the file-in files, if any, are copied to the VM

Next, the script file is coped to the remote machine and executed.
If multiple --script are specified they will be executed in the order given.

Finally, the file-out files, if any, are copied from the VM.

The file-in, and file-out options support a from:to colon-separated syntax:

```
	--file-in=./src/foo.c:/tmp/foo.c
	--file-out=/tmp/foo.o:./build/foo.o
```

If a colon is not specified, then the working directory and basename is
used for the target.

## Dependencies

It uses qemu, the Debian cloud image, genisoimage, gnu-make.
Here is a (possibly incomplete) apt command for debian systems:

```
    sudo apt install make qemu-system-x86 qemu-utils genisoimage iproute2
```

## Running the demo

The acceptance test serves as a demonstration of usage.
You can run the test just by running make in this directory, e.g.

    make

There should be files called `build/message1.txt` and `build/message2.txt`
when the build is complete that were generated from within the vm.
