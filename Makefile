# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2022 S. K. Medlock, E. K. Herman, K. M. Shaw

SHELL=/bin/bash

.PHONY: all
all: check

.PHONY: clean
clean:
	rm -rfv build vm

.PHONY: distclean
distclean: clean
	rm -rfv *.qcow2 python-venv cloud-images

cloud-images/debian-cloud.qcow2: build-in-vm
	./build-in-vm --just-download-cloud-qcow2

vm/blank/blank.qcow2.sh: build-in-vm cloud-images/debian-cloud.qcow2
	./build-in-vm --vm-hostname=blank --preserve-qcow2 \
		--vm-ssh-user=root \
		--vm-ssh-user-hashed-passwd=$$(mkpasswd foo)

build/bios.txt: \
		build-in-vm cloud-images/debian-cloud.qcow2 \
		test/bootmode.sh
	./build-in-vm \
		--vm-hostname=boot-me-bios \
		--script=test/bootmode.sh \
		--file-out=bootmode.txt:build/bios.txt

build/uefi.txt: \
		build-in-vm cloud-images/debian-cloud.qcow2 \
		test/bootmode.sh
	./build-in-vm \
		--uefi \
		--vm-hostname=boot-me-uefi \
		--script=test/bootmode.sh \
		--file-out=bootmode.txt:build/uefi.txt

vm/alfred/alfred.qcow2 build/message1.txt build/message2.txt &: \
		build-in-vm cloud-images/debian-cloud.qcow2 \
		test/build-messages.sh test/in1.txt test/in2.txt
	./build-in-vm \
		--unrestricted \
		--vm-hostname=alfred \
		--file-in=test/in1.txt:in1.txt \
		--file-in=test/in2.txt:in2.txt \
		--script=test/build-messages.sh \
		--script=test/bootmode.sh \
		--file-out=message1.txt:build/message1.txt \
		--file-out=message2.txt:build/message2.txt \
		--preserve-qcow2

vm/failing-build/failing-build.qcow2 \
vm/failing-build/failing-build.qcow2.sh &: \
		build-in-vm cloud-images/debian-cloud.qcow2 \
		test/fail.log.txt.expected \
		test/fail.sh
	@echo "This build is expected to fail, and not cleanup"
	@echo " even though we do not specify --preserve-qcow2"
	./build-in-vm \
		--vm-hostname=failing-build \
		--script=test/fail.sh \
		--file-out-always=log.txt:build/log.txt \
		|| true
	diff -u test/fail.log.txt.expected build/log.txt

.PHONY: check-hashed-passwd-requires-root
check-hashed-passwd-requires-root: build-in-vm cloud-images/debian-cloud.qcow2
	./build-in-vm --vm-hostname=pwtest --preserve-qcow2 \
		--vm-ssh-user=bob \
		--vm-ssh-user-hashed-passwd=$$(mkpasswd foo) \
		2>&1 | grep "root"
	@echo SUCCESS $@

.PHONY: check-failing-build-preserves-vm
check-failing-build-preserves-vm: vm/failing-build/failing-build.qcow2.sh
	vm/failing-build/failing-build.qcow2.sh exit
	@echo SUCCESS $@

.PHONY: check-message1
check-message1: build/message1.txt test/message1.txt.check
	diff -u $^
	@echo SUCCESS $@


.PHONY: check-message2
check-message2: build/message2.txt test/message2.txt.check
	diff -u $^
	@echo SUCCESS $@

.PHONY: check-bios-boot
check-bios-boot: build/bios.txt test/bios.txt.check
	diff -u $^
	@echo SUCCESS $@

.PHONY: check-uefi-boot
check-uefi-boot: build/uefi.txt test/uefi.txt.check
	diff -u $^
	@echo SUCCESS $@

.PHONY: check-blank
check-blank: vm/blank/blank.qcow2.sh
	vm/blank/blank.qcow2.sh exit
	@echo SUCCESS $@

.PHONY: check
check: check-blank \
		check-message1 check-message2 \
		check-bios-boot check-uefi-boot \
		check-hashed-passwd-requires-root \
		check-failing-build-preserves-vm
	@echo SUCCESS $@

python-venv/bin/activate: requirements.txt
	rm -rf python-venv
	python3 -m venv python-venv
	source python-venv/bin/activate && \
	 pip3 install --upgrade pip
	source python-venv/bin/activate && \
	 pip3 install -r requirements.txt

.PHONY: update
update: update-cloud-info.py python-venv/bin/activate
	source python-venv/bin/activate && \
	 python3 ./$<

.PHONY: black
black: update-cloud-info.py python-venv/bin/activate
	source python-venv/bin/activate && \
	 black $<
