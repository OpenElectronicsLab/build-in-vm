#!/bin/bash
# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2024 Stichting Open Electronics Lab

set -e
set -x
set -o pipefail

git fetch --all
git reset --hard origin/main

make update

if [ "$(git status --untracked-files=no --short | wc --lines)" -eq 0 ]; then
	echo "no changes"
	exit 0
fi

make check

git add build-in-vm
git commit -m "automated update $(date --utc +%Y%m%dT%H%M%SZ)"

git push
